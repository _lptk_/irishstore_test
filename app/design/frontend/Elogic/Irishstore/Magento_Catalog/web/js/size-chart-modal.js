define([
    'jquery',
    'Magento_Ui/js/modal/modal',
], function ($, modal) {
    'use strict';

    $.widget('irishstore.sizeChartModal', {
        options: {
            modalContainer:  '#size_chart_popup',
            modalOptions: {
                type: 'popup',
                modalClass: 'custom-modal size-chart-modal',
                title: '',
                opened: function () {
                    // do smth
                },
                closed: function () {
                    // do smth
                },
                buttons: [{
                    text: $.mage.__('CLOSE'),
                    class: '',
                    click: function () {
                        this.closeModal();
                    }
                }]
            }
        },

        /**
         * @inheritdoc
         */
        _create: function () {
            var sizeChart = $(this.element);
            var options = this.options;
            var modalOptions = options.modalOptions;
            var modalContainer = $(document).find(options.modalContainer);
            modal(modalOptions, modalContainer);
            sizeChart.on('click',function() {
                modalContainer.modal('openModal');
            });
        }
    });

    return $.irishstore.sizeChartModal;
});
